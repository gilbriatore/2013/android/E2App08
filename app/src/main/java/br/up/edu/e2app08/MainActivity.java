package br.up.edu.e2app08;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular(View v){

        EditText cxRaio = (EditText) findViewById(R.id.txtRaio);
        EditText cxAltura = (EditText) findViewById(R.id.txtAltura);
        EditText cxVolume = (EditText) findViewById(R.id.txtVolume);

        double raio = Double.parseDouble(cxRaio.getText().toString());
        double altura = Double.parseDouble(cxAltura.getText().toString());

        double volume = 3.14 * Math.pow(raio, 2) * altura;

        cxVolume.setText(String.format("%.0f", volume));
 }
}